package com.example.okorchysta.imageanalyzer.Common;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by OKorchysta on 14.11.2017.
 */

public class Image implements Serializable {

    String id;
    String title;
    long datetime;
    long views;
    long points;
    String link;
    String path;


    public Image(String id, String title, long datetime, long views, long points, String link, String path) {
        this.id = id;
        this.title = title;
        this.datetime = datetime;
        this.views = views;
        this.points = points;
        this.link = link;
        this.path = path;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", datetime=" + datetime +
                ", views=" + views +
                ", points=" + points +
                ", link='" + link + '\'' +
                ", path='" + path + '\'' +
                '}';
    }

}
