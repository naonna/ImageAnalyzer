package com.example.okorchysta.imageanalyzer;

/**
 * Created by OKorchysta on 14.11.2017.
 */

public class ResultObject {

    public enum CODE{

        //TODO response codes from imgur
        OK(""),
        IMGUR_ERROR("");

        private String errorMessage;

        CODE(String errorMessage){
            this.errorMessage  = errorMessage;
        }

        public String getErrorMessage(){
            return errorMessage;
        }
    }

    CODE code;
    Object result;

    public ResultObject(CODE code, Object result){
        this.code = code;
        this.result = result;

    }

    public CODE getCode() {
        return code;
    }

    public Object getResult() {
        return result;
    }
}
