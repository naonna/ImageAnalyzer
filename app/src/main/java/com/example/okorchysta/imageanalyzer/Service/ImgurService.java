package com.example.okorchysta.imageanalyzer.Service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.okorchysta.imageanalyzer.Common.Constants;
import com.example.okorchysta.imageanalyzer.Common.Image;
import com.example.okorchysta.imageanalyzer.ResultObject;
import com.example.okorchysta.imageanalyzer.Service.presenter.IServicePresenter;
import com.example.okorchysta.imageanalyzer.Service.presenter.ServicePresenter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by OKorchysta on 13.11.2017.
 */

public class ImgurService extends IntentService implements IServiceView {

    public static final String TAG = "ImgurService";

    IServicePresenter servicePresenter = new ServicePresenter(this);
    ResultReceiver resultReceiver;

    public ImgurService() {
        super("myname");
    }

    public ImgurService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, "---->onHandleIntent");
        if (intent!=null){
            resultReceiver = intent.getParcelableExtra("receiver");
        }
        getImgurPage(0);

        Log.d(TAG, "onHandleIntent<----");

    }


    @Override
    public void getImgurPage(int page) {
        Log.d(TAG, "---->getDataFromImgurPage: ");
        String urlString = "https://api.imgur.com/3/gallery/top/viral/day/"+page+"?showViral=true&mature=false&album_previews=true";

        ResultObject resultObject = null;

        try {
            URL url = new URL(urlString);
            resultObject = servicePresenter.getDataFromImgurPage(url);
        } catch (MalformedURLException ex){
            Log.d(TAG, "Exception during getting url from String: " + ex);
        }

        if(resultObject!= null && resultObject.getCode().equals(ResultObject.CODE.OK)){
            Bundle bundle = new Bundle();
            bundle.putSerializable("list", (ArrayList<Image>) resultObject.getResult());
            resultReceiver.send(Constants.SERVICE_RECEIVE_LIST, bundle);
        } else {
            resultReceiver.send(Constants.SERVICE_ERROR, null);
        }

        Log.d(TAG, "getDataFromImgurPage<----");

    }
}
