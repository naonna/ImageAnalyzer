package com.example.okorchysta.imageanalyzer.Service.presenter;

import android.util.Log;

import com.example.okorchysta.imageanalyzer.Common.Image;
import com.example.okorchysta.imageanalyzer.ResultObject;
import com.example.okorchysta.imageanalyzer.Service.model.IServiceModel;
import com.example.okorchysta.imageanalyzer.Service.IServiceView;
import com.example.okorchysta.imageanalyzer.Service.model.ServiceModel;

import org.json.JSONArray;

import java.net.URL;
import java.util.List;

/**
 * Created by OKorchysta on 14.11.2017.
 */

public class ServicePresenter implements IServicePresenter {

    public static final String TAG = "ServicePresenter";

    IServiceView serviceView;
    IServiceModel serviceModel;

    public ServicePresenter(IServiceView serviceView){
        this.serviceView = serviceView;
        serviceModel = new ServiceModel();
    }


    @Override
    public ResultObject getDataFromImgurPage(URL url) {
        Log.d(TAG, "---->getDataFromImgurPage: ");

        ResultObject resultObject = serviceModel.getDataFromImgurPage(url);
        if (resultObject.getCode().equals(ResultObject.CODE.OK)){

            List<Image> imageList = getImageList((JSONArray) resultObject.getResult());

            Log.d(TAG, "Return List<Image>");
            Log.d(TAG, "getDataFromImgurPage<----");
            return new ResultObject(ResultObject.CODE.OK, imageList);
        } else {
            Log.d(TAG, "Error. Return null");
            Log.d(TAG, "getDataFromImgurPage<----");
            return new ResultObject(ResultObject.CODE.IMGUR_ERROR, null);
        }
    }

    @Override
    public List<Image> getImageList(JSONArray jsonArray){
        Log.d(TAG, "---->getImageList<----");
        return serviceModel.getImageList(jsonArray);
    }



}
