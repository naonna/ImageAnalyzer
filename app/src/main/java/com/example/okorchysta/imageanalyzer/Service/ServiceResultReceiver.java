package com.example.okorchysta.imageanalyzer.Service;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import com.example.okorchysta.imageanalyzer.Client.IMainView;
import com.example.okorchysta.imageanalyzer.Common.Constants;

/**
 * Created by OKorchysta on 15.11.2017.
 */

public class ServiceResultReceiver extends ResultReceiver {

    IMainView mainView;

    public ServiceResultReceiver(IMainView mainView) {
        super(new Handler());
        this.mainView = mainView;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode){
            case Constants.SERVICE_ERROR :
                mainView.onError();
                break;
            case Constants.SERVICE_RECEIVE_LIST :
                mainView.onSuccess(resultData);
                break;
        }


        super.onReceiveResult(resultCode, resultData);
    }
}
