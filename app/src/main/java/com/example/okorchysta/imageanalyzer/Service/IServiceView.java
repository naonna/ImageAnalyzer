package com.example.okorchysta.imageanalyzer.Service;

import java.net.MalformedURLException;

/**
 * Created by OKorchysta on 14.11.2017.
 */

public interface IServiceView {

    void getImgurPage(int page) throws MalformedURLException;

}
